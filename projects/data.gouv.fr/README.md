# DataGouvFr

This library aims to offer a Typoescrit/js level API to access data.gouv.fr API.

## Code scaffolding

Run `ng generate component component-name --project data.gouv.fr` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project data.gouv.fr`.
> Note: Don't forget to add `--project data.gouv.fr` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build data.gouv.fr` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build data.gouv.fr`, go to the dist folder `cd dist/data.gouv.fr` and run `npm publish`.

## Running unit tests

Run `ng test data.gouv.fr` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
