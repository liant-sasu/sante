import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataGouvFrComponent } from './data.gouv.fr.component';

describe('DataGouvFrComponent', () => {
  let component: DataGouvFrComponent;
  let fixture: ComponentFixture<DataGouvFrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataGouvFrComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataGouvFrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
