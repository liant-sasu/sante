import { Component } from '@angular/core';

@Component({
  selector: 'dgf-view',
  template: `
    <mat-card class="container">
      data.gouv.fr works!
    </mat-card>
  `,
  styles: [
    '.container { height: 100px; width: 100px; }'
  ]
})
export class DataGouvFrComponent {

}
