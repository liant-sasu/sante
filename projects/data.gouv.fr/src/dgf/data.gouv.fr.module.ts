import { NgModule } from '@angular/core';
import { DataGouvFrComponent } from './data.gouv.fr.component';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [
    DataGouvFrComponent
  ],
  imports: [
    HttpClientModule,
    MatCardModule
  ],
  exports: [
    DataGouvFrComponent
  ]
})
export class DataGouvFrModule { }
