import { Injectable } from '@angular/core';
import {
  HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpErrorResponse
} from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';

const BASE_URL = "https://www.data.gouv.fr/api/1/";

@Injectable({
  providedIn: 'root'
})
export class DataGouvFrService {

  constructor(protected http_client: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }

  headers(headers?: HttpHeaders | string | { [name: string ]: string | string[] ;}): HttpHeaders {
    if(!(headers instanceof HttpHeaders)) {
      headers = new HttpHeaders(headers);
    }
    if(!headers.has('accept')) {
      headers.append('accept', 'application/json')
    }
    return headers;
  }

  get<X>(path: string, headers?: HttpHeaders | {[header: string]: string | string[]},
    params?: HttpParams|{[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>},
    reportProgress?: boolean,
    withCredentials?: boolean,
  ): Observable<HttpEvent<X>> {
    let options = {
      headers: this.headers(headers),
      responseType: 'json' as const,
      observe: 'events' as const,
      // Copy the rest of the request options
      params: params, reportProgress: reportProgress, withCredentials: withCredentials
    };
    return this.http_client.get<X>(BASE_URL + path, options)
      .pipe(
        catchError(this.handleError)
      );
  }
}
