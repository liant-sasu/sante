import { TestBed } from '@angular/core/testing';

import { DataGouvFrService } from './data.gouv.fr.service';

describe('DataGouvFrService', () => {
  let service: DataGouvFrService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataGouvFrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
