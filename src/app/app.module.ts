import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatCardModule } from '@angular/material/card';
import { DataGouvFrModule } from 'data.gouv.fr';

import { SiteComponent } from './site/site.component';
import { SiteRoutingModule } from './site/site-routing.module';
import { SiteModule } from './site/site.module';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    RouterModule,
    SiteRoutingModule,
    MatCardModule,
    DataGouvFrModule,
    SiteModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [SiteComponent]
})
export class AppModule { }
